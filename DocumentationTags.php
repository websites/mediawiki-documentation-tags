<?php
if ( !defined( 'MEDIAWIKI' ) ) die();
/**
 * @file
 * @ingroup Extensions
 *
 * @author Niklas Laxström
 * @copyright Copyright © 2010, Niklas Laxström
 */

$wgExtensionCredits['parser'][] = array(
	'path'           => __FILE__,
	'name'           => 'Documentation Tags',
	'version'        => 1.0,
	'author'         => array( 'Niklas Laxström' ),
	'description'    => 'Provides tags for commonly used features in documentation',
);

// Setup class autoloads
$dir = dirname( __FILE__ ) . '/';
$wgHooks['ParserFirstCallInit'][] = 'efInitDocuTags';

$wgDocumentationTags = array();

function efInitDocuTags( $parser ) {
	global $wgDocumentationTags;

	$render = new TagRenderer();
	foreach ( $wgDocumentationTags as $tag ) {
		$parser->setHook( $tag, array( $render, $tag ) );
	}
	return true;
}

class TagRenderer {
	public function __call( $name, $args ) {
		return Html::element( 'span', array( 'class' => $name ), $args[0] );
	}
}